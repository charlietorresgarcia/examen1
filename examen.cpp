// Online C++ Compiler - Build, Compile and Run your C++ programs online in your favorite browser

#include<iostream>
#include<cmath>
using namespace std;
// Pregunta 1: Complete la declaraci�n de la clase base Figura
class Figura {
public:
virtual double calcularArea() = 0;
};
// Pregunta 2: Complete la declaraci�n de la clase Triangulo que hereda de Figura
class Triangulo : public Figura {
private:
// Atributos
double lado1;
double lado2;
double lado3;
public:
// Constructor
// M�todo para calcular el �rea
Triangulo(double lado1, double lado2, double lado3) : lado1(lado1), lado2(lado2), lado3(lado3) {}
double calcularArea() override {
    //usamos la formula de Heron para calcular el �rea de un Triangulo
    double s= (lado1 + lado2 + lado3) / 2;
    double area = sqrt(s * (s-lado1)* (s - lado2) * (s -lado3));
    return area;
}
};
// Pregunta 3: Complete la declaraci�n de la clase Cuadrado que hereda de Figura
class Cuadrado : public Figura {
private:
// Atributo
double lado;
public:
// Constructor
// M�todo para calcular el �rea
Cuadrado(double lado) : lado(lado) {}
double calcularArea() override {
    return lado * lado; //area del cuadrado = lado * lado
}
};
// Pregunta 4: Complete el c�digo en la funci�n main
int main() {
// Crear instancias de Triangulo y Cuadrado
// Mostrar el c�lculo del �rea de cada uno
Triangulo triangulo(4.0, 5.0, 6.0);  // Ejemplo de un tri�ngulo con lados de longitud 4, 5 y 6.
    Cuadrado cuadrado(4.0);             // Ejemplo de un cuadrado con un lado de longitud 4.

    double areaTriangulo = triangulo.calcularArea();
    double areaCuadrado = cuadrado.calcularArea();

    std::cout << "�rea del tri�ngulo: " << areaTriangulo << std::endl;
    std::cout << "�rea del cuadrado: " << areaCuadrado << std::endl;


    return 0;
}

// Explica brevemente en que consiste la herencia en c++ y cuales son sus beneficios en la programacion orientada a objetos:
//La herencia en C++ es un concepto de la programaci�n orientada a objetos que permite que una clase (clase derivada) herede propiedades y comportamientos de otra clase (clase base). Los beneficios de la herencia en la programaci�n orientada a objetos incluyen la reutilizaci�n de c�digo, la capacidad de extender y personalizar clases base, el polimorfismo para tratar objetos de manera uniforme, una organizaci�n estructurada de clases, abstracci�n para modelar conceptos y facilitar la resoluci�n de problemas.
